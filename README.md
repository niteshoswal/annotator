### Annotator

The project has been developed & tested on `node@10`. And used [yarn](https://yarnpkg.com/lang/en/) as it's package manager.

To run the project, navigate to the directory in which the project was cloned & run the following commands -
```bash
yarn install
yarn start
``` 
