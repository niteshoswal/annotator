import React, { useState, useEffect } from 'react';
import 'antd/dist/antd.css';
import Editor from './components/Editor';
import Export from './components/Export';
import { Row, Col, Button, Icon } from 'antd';

const styles = {
  centerColumn: {
    textAlign: 'center'
  }
};

function App() {
  const [output, setOutput] = useState(new Map()); // This will handle all ops
  const [index, setIndex] = useState(''); // For checking which index we're on

  /**
   * Handle output changes
   * @param key
   * @param value
   */
  const handleOutputChange = (key, value) => {
    // Creating a map is faster, because of lookups and sequential looping, and setState diff works faster
    setOutput(new Map(output.set(index, Object.assign({
      ...output.get(index),
      [key]: value,
    }))));
  };

  const goToPrevious = () => {
    const ids = [...output.keys()];
    const position = ids.indexOf(index);
    if(position > 0) {
      setIndex(ids[position - 1]);
    }
  };

  const goToNext = () => {
    const ids = [...output.keys()];
    const position = ids.indexOf(index);
    if(position !== ids.length - 1) {
      setIndex(ids[position + 1]);
    }
  };

  /**
   * Data seeder, basically
   * Request for the input, ideally would be called to fetch data from an API or from a user action (upload?)
   */
  useEffect(() => {
    (async () => {
      const response = await fetch('/input.json', {
        method: "GET"
      });
      /*
       * Map the response to what the application wants, keep the thing consistent if we change the data source
       */
      const transformed = new Map();
      (await response.json()).forEach((x, i) => {
        if(i === 0) {
          // Set the first object's Id as index.
          setIndex(x.Id);
        }
        transformed.set(x.Id, {
          prevText: x.prev_text,
          nextText: x.next_text,
          text: x.text,
          label: '',
          id: x.Id,
        });
      });
      setOutput(transformed);
    })();
  }, []);

  return (
    <div className="App">
      <Row justify="center" type="flex" align="middle">
        <Col span={4} style={styles.centerColumn}>
          <Button
            size="large"
            disabled={!([...output.keys()].indexOf(index) > 0)}
            onClick={goToPrevious}
          >
            <Icon type="left" /> Previous
          </Button>
        </Col>
        <Col span={16}>
          <Editor input={output.get(index)} onChange={handleOutputChange} />
        </Col>
        <Col span={4} style={styles.centerColumn}>
          <Button
            size="large"
            disabled={[...output.keys()].indexOf(index) === (output.size - 1)}
            onClick={goToNext}
          >
            Next <Icon type="right" />
          </Button>
        </Col>
      </Row>
      {([...output.keys()].indexOf(index) === (output.size - 1)) ? (
        <>
          <Row>
            <Col span={12} offset={6}>
              <hr/>
              <Export data={output} />
            </Col>
          </Row>
        </>
      ) : null}
    </div>
  );
}

export default App;
