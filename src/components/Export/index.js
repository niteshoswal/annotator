import React from "react";
import PropTypes from "prop-types";
import { Button, Typography } from "antd";
import fileSaver from "file-saver";
const { Text } = Typography;

function ExportComponent(props) {

  /**
   * Handles the export of the annotated data
   */
  const handleExport = () => {
    console.log(props.data);
    const blob = new Blob([JSON.stringify([...props.data.values()].map(x => ({
      prev_text: x.prevText,
      next_text: x.nextText,
      text: x.text,
      Id: x.id,
      label: x.label
    })))], {
      type: "text/json;charset=utf-8"
    });
    fileSaver.saveAs(blob, 'output.json');
  };

  return (
    <>
      <Text>Once you're done, export your changes by clicking the button here </Text>
      {' '}
      <Button type="danger" onClick={handleExport}>Export</Button>
    </>
  );
};

ExportComponent.propTypes = {
  data: PropTypes.arrayOf(PropTypes.shape({
    prevText: PropTypes.string,
    text: PropTypes.string.isRequired,
    nextText: PropTypes.string,
    label: PropTypes.string,
    id: PropTypes.string
  }))
};

export default ExportComponent;
