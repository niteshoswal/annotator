import React, { useState } from "react";
import PropTypes from "prop-types";
import { Form, Input, Row, Col, Typography, Icon, Button, Radio, Alert } from "antd";
import labels from "./labels";

const { Title } = Typography;


function EditorComponent(props) {
  const [buttonState, setButtonState] = useState({
    prevToText: {
      state: false,
      start: null,
      end: null,
    },
    textToPrev: {
      state: false,
      start: null,
      end: null,
    },
    textToNext: {
      state: false,
      start: null,
      end: null,
    },
    nextToText: {
      state: false,
      start: null,
      end: null,
    },
  });

  /**
   * Handle button state change based on what's selected
   * @param button
   * @param state
   */
  const handleButtonStateChange = (button, state) => {
    setButtonState(Object.assign({}, buttonState, {
      [button]: state
    }));
  };

  /**
   * Reset button state back to initial state
   * @param button
   */
  const resetButtonState = (button) => {
    // Reset button
    handleButtonStateChange(button, {
      state: false,
      start: null,
      end: null
    });
  };

  /**
   * Handle the cut operation
   * @param button
   */
  const handleCut = (button) => {
    if(!buttonState[button].state) {
      return; // Don't do anything if the state is false.
    }
    let text = '';
    const { start, end } = buttonState[button];
    // Will probably be a switch
    switch (button) {
      case 'textToPrev':
        text = props.input.text.substring(start, end);
        props.onChange('prevText', `${props.input.prevText}${text}`);
        props.onChange('text', props.input.text.substring(end, props.input.text.length));
        break;
      case 'textToNext':
        text = props.input.text.substring(start, end);
        props.onChange('nextText', `${text}${props.input.nextText}`);
        props.onChange('text', props.input.text.substring(0, start));
        break;
      case 'prevToText':
        text = props.input.prevText.substring(start, end);
        props.onChange('text', `${text}${props.input.text}`);
        props.onChange('prevText', props.input.prevText.substring(0, start));
        break;
      case 'nextToText':
        text = props.input.nextText.substring(start, end);
        props.onChange('text', `${props.input.text}${text}`);
        props.onChange('nextText', props.input.nextText.substring(end, props.input.nextText.length));;
        break;
      default:
        // Do nothing. Also, highly unlikely
    }
    resetButtonState(button);
  };

  if(!props.input) {
    return (
      <div>
        <Row align="middle" justify="space-around">
          <Col span={8} offset={8} style={{
            textAlign: 'center'
          }}>
            <Title level={2}><Icon type="smile" /> Nothing left to annotate!</Title>
          </Col>
        </Row>
      </div>
    );
  }
  return (
    <>
      <Row>
        <Col span={24}>
          <Alert message={
            "All changes are auto-saved. You can navigate across through different sentences " +
            "using the \"Next\" and \"Previous\" buttons."
          } type="info" />
          <Form>
            <Form.Item label="Preceding Text">
              <Input.TextArea
                rows={4}
                value={props.input.prevText}
                readOnly={true}
                onSelect={(event) => {
                  resetButtonState('prevToText');
                  if(event.target.selectionStart !== event.target.selectionEnd && event.target.selectionEnd === props.input.prevText.length) {
                    handleButtonStateChange('prevToText', {
                      state: true,
                      start: event.target.selectionStart,
                      end: event.target.selectionEnd
                    });
                  }
                }}
              />
              <Button
                type="primary"
                disabled={!buttonState.prevToText.state}
                onClick={() => {
                  handleCut('prevToText');
                }}
                block
              >
                {buttonState.prevToText.state ? "Cut to \"Text\"" : "Select text from the end to cut to \"Text\"" }
                <Icon type="down" />
              </Button>
            </Form.Item>

            <Form.Item label="Text">
              <Button
                type="primary"
                disabled={!buttonState.textToPrev.state}
                onClick={() => {
                  handleCut('textToPrev');
                }}
                block
              >
                {buttonState.textToPrev.state ? "Cut to \"Preceding Text\"" : "Select text from the beginning to cut to \"Preceding Text\"" }
                <Icon type="up" />
              </Button>
              <Input.TextArea
                rows={4}
                value={props.input.text}
                readOnly={true}
                onSelect={(event) => {
                  resetButtonState('textToNext');
                  resetButtonState('textToPrev');
                  if(event.target.selectionStart !== event.target.selectionEnd) {
                    if (event.target.selectionEnd === props.input.text.length) {
                      handleButtonStateChange('textToNext', {
                        state: true,
                        start: event.target.selectionStart,
                        end: event.target.selectionEnd
                      });
                    }
                    if (event.target.selectionStart === 0) {
                      handleButtonStateChange('textToPrev', {
                        state: true,
                        start: event.target.selectionStart,
                        end: event.target.selectionEnd
                      });
                    }
                  }
                }}
              />
              <Button
                type="primary"
                disabled={!buttonState.textToNext.state}
                onClick={() => {
                  handleCut('textToNext');
                }}
                block
              >
                {buttonState.textToNext.state ? "Cut to \"Following Text\"" : "Select text from the end to cut to \"Following Text\"" }
                <Icon type="down" />
              </Button>
            </Form.Item>
            <Form.Item label="Following Text">
              <Button
                type="primary"
                disabled={!buttonState.nextToText.state}
                onClick={() => {
                  handleCut('nextToText');
                }}
                block
              >
                {buttonState.nextToText.state ? "Cut to \"Text\"" : "Select text from the beginning to cut to \"Text\"" }
                <Icon type="up" />
              </Button>
              <Input.TextArea
                rows={4}
                value={props.input.nextText}
                readOnly={true}
                onSelect={(event) => {
                  resetButtonState('nextToText');
                  if(event.target.selectionStart !== event.target.selectionEnd && event.target.selectionStart === 0) {
                    handleButtonStateChange('nextToText', {
                      state: true,
                      start: event.target.selectionStart,
                      end: event.target.selectionEnd
                    });
                  }
                }}
              />
            </Form.Item>
            <Form.Item label="Label">
              <Radio.Group
                options={labels}
                onChange={(event) => {
                  props.onChange('label', event.target.value);
                }}
                value={props.input.label}
              />
            </Form.Item>
          </Form>
        </Col>
      </Row>
    </>
  );
}

EditorComponent.propTypes = {
  input: PropTypes.shape({
    prevText: PropTypes.string,
    text: PropTypes.string.isRequired,
    nextText: PropTypes.string,
    label: PropTypes.string,
    id: PropTypes.string
  }),
  onChange: PropTypes.func.isRequired,
};

const WrappedEditorComponent = Form.create({name: 'annotate'})(EditorComponent);

export default WrappedEditorComponent;
