const labels = [
  "Amendments",
  "Arbitration/Cour Prorogation",
  "Assignment",
  "Change of Control",
  "Confidentiality",
  "Contractual Penalties",
  "Default Interest",
  "Governing Law",
  "Guarantees",
  "Indemnification",
  "License",
  "Non-Solicitation",
  "Parties",
  "Payment Interest",
  "Set-off",
  "Waivers",
  "Zero Tag"
];

export default labels;
